const _ = require('lodash')

module.exports.mapObjectWithReference = (ref, source) => {
    const keys = _.keys(source)
    const target = {}
    const targetList = []
  
    for (const key of keys) {
        if (ref[key]){
            target[ref[key]] = source[key]
        }
    }
  
    return target
  }