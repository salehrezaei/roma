## Running Locally

```
git clone https://gitlab.com/salehrezaei/roma.git
cd roma
npm install
change the database name and username and password in the .env file
npm run migration
npm start
for test, stop the server and run npm test
