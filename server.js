const express = require('express')
const bodyParser = require('body-parser')
const dotenv = require('dotenv')
dotenv.config()

const twitRouter = require('./routes/twit')

const app =  express()
const port = process.env.PORT

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.use('/api/twits', twitRouter)

app.listen(port, () => {
    console.log(`Server is running on port ${port}.`)
})

module.exports = app;