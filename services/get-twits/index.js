const superagent = require('superagent')
require('dotenv').config()

const address = process.env.SERVICE

module.exports.get = async () => {
  let t = superagent.get(`${address}`)
  try {
    let result = await t
      .set('User-Agent', 'Chrome/61')
      
    if (result && result.text) {
        result = JSON.parse(result.text)
        return result.items
    } else {
      return false
    }
  } catch (e) {
    throw e
  }
}
