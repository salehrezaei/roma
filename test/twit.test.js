const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../server')
let should = chai.should() 
var expect = require('chai').expect;

chai.use(chaiHttp);

describe('/Post twits', () => {
  it('it should get twits and save them', (done) => {
    chai.request(server)
      .post('/api/twits')
      .end((err, res) => {
        res.body.should.be.a('array')
        done();
      })
  })
})

describe('/Get twit by sender name', () => {
  it('it should return an array', (done) => {
    chai.request(server)
      .get('/api/twits/chaii')
      .end((err, res) => {
        res.body.should.be.a('array')
        done();
      })
  })
})

describe('/Delete twit by id', () => {
  it('it should delete twit', (done) => {
    chai.request(server)
      .delete('/api/twits/24')
      .end((err, res) => {
        res.body.should.have.property('message')
        done();
      })
  })
})