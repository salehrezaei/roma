exports.up = knex =>
  knex.schema.createTable("TWITS", table => {
    table.increments('ID').primary()
    table.string('SENDER_USERNAME', 30).notNullable()
    table.string('TYPE', 10).notNullable()
    table.string('CONTENT', 1000).notNullable()
    table.string('SEND_TIME', 30).notNullable()
  });

 exports.down = knex => knex.schema.dropTableIfExists("TWITS");
