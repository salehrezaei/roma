require('dotenv').config();

module.exports = {
  development: {
    client: 'pg',
    connection: {
      database: process.env.DATABASE,
      user: process.env.DB_USER,
      password: process.env.PASSWORD
    },
    migrations: {
      directory: './data/migrations',
      tableName: 'MIGRATIONS'
    }
  },
  production: {
    client: 'pg',
    connection: {
      database: process.env.DATABASE,
      user: process.env.DB_USER,
      password: process.env.PASSWORD
    },
    migrations: {
      directory: './data/migrations',
      tableName: 'MIGRATIONS'
    }
  },
};


