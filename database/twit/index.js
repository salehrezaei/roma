const _ = require('lodash')
const db = require('../../data/db')
const util = require('../../util')

const tableName = 'TWITS'

const fields = {
    id: 'ID',
    senderUsername: 'SENDER_USERNAME',
    type: 'TYPE',
    content: 'CONTENT',
    sendTime: 'SEND_TIME'
}

module.exports.fields = fields
  
  module.exports.select = async (conditions) => {
    conditions = util.mapObjectWithReference(fields, conditions)
    const keys = Object.keys(conditions)
    let query = db(tableName).select(fields)
    for (const key of keys) {
      const value = conditions[key]
      if (_.isArray(value)) {
        query.whereIn(key, value)
      } else {
        query.where(key, value)
      }
    }
    return query
  }
  
  module.exports.save = async (params) => {
    params = util.mapObjectWithReference(fields, params)
    const result = await db(tableName).insert(params)
    if (result && result.rowCount === 1){
      return params
    } else {
      return result
    }
  }

  module.exports.delete = async (id) => {
    return db(tableName)
      .where({ID: id}).del()
  }