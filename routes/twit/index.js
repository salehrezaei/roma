const express = require('express')
const moment = require('moment-jalaali')
const _ = require('lodash')

const TWIT = require('../../database/twit')
const GetTwits = require('../../services/get-twits')

const router = express.Router();

router.get('/', async (req, res) => {
    const twits = await TWIT.select();
    res.json(twits)
})

router.get('/:username', async (req, res) => {
    const senderUsername = req.params.username

    const twits = await TWIT.select({ senderUsername })
    res.json(twits)
})

router.post('/', async (req, res) => {
    let twits = await GetTwits.get()

   if (twits)
{
    const now = moment().format('jYYYY/jMM/jDD HH:mm')
    const minuteAgo = moment().subtract(1, 'minute').format('jYYYY/jMM/jDD HH:mm')

    //find twits, retwits and quotes for last minute
    twits = _.filter(twits, (twit) => {
        return ['twit', 'retwit', 'quote'].includes(twit.type) && 
            (twit.sendTimePersian >= minuteAgo && twit.sendTimePersian <= now)
      })

      twits = _.map(twits, )

      const insertedTwits = []
      if (twits.length > 0){
        for await (const twit of twits){
          delete twit.id
          const result = await TWIT.save(twit)
          if (result){
            insertedTwits.push(result)
          }
        }
      } 
      res.json(insertedTwits)
    } else {
        res.status(400).json({message: "Couldn't get twits"})
    }
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id

    const result = await TWIT.delete(id)
    if (result){
    res.json({message: "Twit deleted."})
    } else {
        res.json({message: "No twit deleted"})
    }
})

module.exports = router